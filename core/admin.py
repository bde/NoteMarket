from django.contrib import admin

from .models import Product, Cart, CartItem
# Register your models here.

admin.site.register(Product)

class CartItemInLine(admin.TabularInline):
    model = CartItem
    extra = 1

@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    inlines = [CartItemInLine]
    
