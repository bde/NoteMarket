#!/usr/bin/env python3

from django.urls import path
from .views import CartListView, CartDetailView


urlpatterns = [
    path('carts/', CartListView.as_view()),
    path('carts/<int:pk>', CartDetailView.as_view()),
]
