from .models import Product, Cart, CartItem

from django.views.generic import ListView, DetailView, CreateView


# Create your views here.

class CartListView(ListView):
    model =  Cart
    
class CartDetailView(DetailView):
    model =  Cart
    
