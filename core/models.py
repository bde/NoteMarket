from django.db import models
from django.db.models import Sum
from django.conf import settings
from django.utils.timezone import now

PK_BONVIVENS = 1
PK_AMAP = 2

CLUBS_CHOICE = [
    (PK_BONVIVENS, 'bonvivens'),
    (PK_AMAP,  'amap'),
]

class Product(models.Model):
    """
    A Product is set to sell for commands """
    name = models.CharField(max_length=255,
                            unique=True,
                            )
    
    description = models.CharField(max_length=255)

    # price are store in cents of Euros
    unit_price = models.IntegerField(verbose_name="prix",
                                     default=0,
                                     help_text="",
                                     )

    stock_quantity = models.PositiveIntegerField(verbose_name="quantity",
                                                 name="quantity",
                                                 null=True,  # if NULL, infinite quantity, it is a pre-order product
                                                 help_text = "number of units put to sell",
                                                 )
    returnable_price = models.IntegerField(verbose_name="returnable product price",
                                           null = True,
                                           blank= True)
    club = models.IntegerField(choices=CLUBS_CHOICE)

    def __str__(self):
        return self.name

    @property
    def available_quantity(self):
        """
        Compute the quantity of product still available ie:
        the amount in stock - the amount placed in cart and not yet delivered.
        """
        quantity_in_carts = CartItem.objects.filter(product_pk=self.pk, is_return=False, cart__delivered=False)\
                                            .aggregate(Sum('quantity'))\
                                            .values()
        if self.quantity is  None: 
            return quantity_in_carts
        else:
            return max(self.quantity - quantity_in_carts, 0)

        
    @property
    def to_returns(self):
        """ Compute the returns placed in the Carts, and to be return """
        
        returns_in_carts = CartItem.objects.filter(product_pk=self.pk, is_return=True, cart__delivered=False)\
                                            .aggregate(Sum('quantity'))\
                                            .values()
        return returns_in_carts
        
class Cart(models.Model):
    """
    A Cart is a list of products requested by a User.
    such command will be checked and validated by a human at the moment of delivery.
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.PROTECT,
                             )
    delivered = models.BooleanField(verbose_name="delivered",
                                   default=False)
    creation_date = models.DateTimeField(verbose_name="Cart Creation date",
                                         default=now)
   
    class Meta:
        verbose_name = 'cart'
        verbose_name_plural = 'carts'
        ordering = ('-creation_date',)

    @property
    def total_price(self):
        return CartItem.objects.filter(cart_pk=self.pk).aggregate(Sum('total_price')).values()
        

class CartItem(models.Model):
    """ An item of a cart"""

    cart = models.ForeignKey(Cart,
                             verbose_name="cart",
                             on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(verbose_name='quantity')
    unit_price = models.IntegerField(verbose_name="unit price"),
    product = models.ForeignKey(Product,
                                verbose_name='product',
                                on_delete=models.PROTECT)
    is_return = models.BooleanField(verbose_name='Return of a item')
    

    @property
    def total_price(self):
        return self.quantity*self.unit_price
    
    class Meta:
        unique_together = ('product','cart', 'is_return')
    
    def save(self,*args,**kwargs):
        if self.is_return:
            self.unit_price = - self.product.returnable_price
        else:
            self.unit_price = self.product.unit_price
        return super().save()

    
