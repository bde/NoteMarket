# NoteMarket

Ceci un site compagnon de la note Kfet, a destination de clubs BDE qui souhaitent mettre en place un systeme de commande, relié a la [NoteKfet](https://gitlab.crans.org/bde/nk20).

L'application  `nk_client` se veux être un exemple de  client pour l'API NoteKfet. 

## Installation 

1. Cloner le dépot 
2. Installer les dépendance
3. faire les migrations
4. runserver

## TODO 

- connexion a la Note (acces solde, vérification adhésion)
- interface club  (Connexion Note avec verification droit club)
- Front 
- utilisation de la note
- installation type prod.


- support d'autre club ? (un seul site commun pour tous les clubs amap, note_market) -> hébergé par le BDE ?

## Cahier des charges 

Un membre du club peux commander au près de son club les biens/services[^1] que ce dernier met en vente. Une commande est a destination d'un club, mais peut regrouper plusieurs produits de ce dernier, ainsi que des retours éventuels de consigne. 

On peux penser 

Ce site simplifie ainsi la vie des clubs (gestion élementaire des stocks, commandes, précommande ec), et automatise les transaction entres les notes de ses membres et ce dernier. La comptabilité du Club est ainsi centralisé sur la note, et évite des systèmes parallèle et asynchrone (bouh les excels sheets)


[^1] coiff[ens] ?

### Nom possibles:

- NoteMarket , bde-market, [ENS]Bay , [ENS]Vente,  March[ENS] ...

#### Clubs intéressé/ interessant

AMAP, Bon vivens , Commande de textile (Pull BDE, Bananes)

