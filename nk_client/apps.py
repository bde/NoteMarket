from django.apps import AppConfig


class NkClientConfig(AppConfig):
    name = 'nk_client'
